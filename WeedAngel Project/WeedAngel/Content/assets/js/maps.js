var map;
var cnt;
var cheater = 0;
var markerCluster;
var mapStyles = [{ "featureType": "road", "elementType": "geometry", "stylers": [{ "lightness": 100 }, { "visibility": "simplified" }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "visibility": "on" }, { "color": "#C6E2FF" }] }, { "featureType": "poi", "elementType": "geometry.fill", "stylers": [{ "color": "#C5E3BF" }] }, { "featureType": "road", "elementType": "geometry.fill", "stylers": [{ "color": "#D1D1B8" }] }];
var clusterStyles = [
    {
        url: '/Content/assets/img/cluster.png',
        height: 36,
        width: 36
    }
];
$(document).ready(function ($) {
    "use strict";

    //  Map in item view --------------------------------------------------------------------------------------------------

    $(".item .mark-circle.map").on("click", function () {
        var _latitude = $(this).closest(".item").attr("data-map-latitude");
        var _longitude = $(this).closest(".item").attr("data-map-longitude");
        var _id = $(this).closest(".item").attr("data-id");
        $(this).closest(".item").find(".map-wrapper").attr("id", "map" + _id);
        var _this = "map" + _id;
        simpleMap(_latitude, _longitude, _this);
        $(this).closest(".item").addClass("show-map");
        $(this).closest(".item").find(".btn-close").on("click", function () {
            $(this).closest(".item").removeClass("show-map");
        });
    });

});

// Simple map ----------------------------------------------------------------------------------------------------------

function simpleMap(_latitude, _longitude, element, markerDrag) {
    if (!markerDrag) {
        markerDrag = false;
    }
    var mapCenter = new google.maps.LatLng(_latitude, _longitude);
    var mapOptions = {
        zoom: 15,
        center: mapCenter,
        disableDefaultUI: true,
        scrollwheel: true,
        styles: mapStyles
    };
    var mapElement = document.getElementById(element);
    var map = new google.maps.Map(mapElement, mapOptions);

    var marker = new MarkerWithLabel({
        position: new google.maps.LatLng(_latitude, _longitude),
        map: map,
        icon: '/Content/assets/img/marker.png',
        labelAnchor: new google.maps.Point(50, 0),
        draggable: markerDrag
    });

    google.maps.event.addListener(marker, "mouseup", function (event) {
        var latitude = this.position.lat();
        var longitude = this.position.lng();
        $('#latitude').val(this.position.lat());
        $('#longitude').val(this.position.lng());
    });

    autoComplete(map, marker);
    weather(_latitude, _longitude);
}

// Weather -------------------------------------------------------------------------------------------------------------

function weather(_latitude, _longitude) {

    if ($(".weather").length) {

        var geocoder;
        var latlng = new google.maps.LatLng(_latitude, _longitude);
        var city, country, street;
        geocoder = new google.maps.Geocoder();

        geocoder.geocode(
            { 'latLng': latlng },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var add = results[0].formatted_address;
                        var value = add.split(",");
                        count = value.length;
                        country = value[count - 1];
                        city = value[count - 2].replace(/\d+/g, '');
                        street = value[count - 3];

                        $.simpleWeather({
                            location: city + ", " + country,
                            woeid: '',
                            unit: 'c',
                            success: function (weather) {
                                var html = '<div class="left"><i class="icon-' + weather.code + '"></i><span>' + weather.temp + '&deg;' + weather.units.temp + '</span>' +
                                    '</div><div class="right"><ul><li>' + weather.city + ', ' + weather.region + '</li><li class="currently">' + weather.currently + '</li></ul></div>';
                                $(".weather-detail").html(html);
                            },
                            error: function (error) {
                                $(".weather-detail").html('<p>' + error + '</p>');
                            }
                        });
                    }
                    else {
                        console.log("address not found");
                    }
                }
                else {
                    console.log("Geocoder failed due to: " + status);
                }
            }
        );
    }
}

//Autocomplete ---------------------------------------------------------------------------------------------------------

function autoComplete(map, marker) {
    if ($("#address-autocomplete").length) {
        var input = /** @type {HTMLInputElement} */(document.getElementById('address-autocomplete'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            if (marker) {
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                $('#latitude').val(marker.getPosition().lat());
                $('#longitude').val(marker.getPosition().lng());
            }
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });

        function success(position) {
            map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            //initSubmitMap(position.coords.latitude, position.coords.longitude);
            $('#latitude').val(position.coords.latitude);
            $('#longitude').val(position.coords.longitude);
        }

        $('.geo-location').on("click", function () {
            if (navigator.geolocation) {
                $('#' + element).addClass('fade-map');
                navigator.geolocation.getCurrentPosition(success);
            } else {
                console.log('Geo Location is not supported');
            }
        });
    }
}

// Big Map on Home -----------------------------------------------------------------------------------------------------
var isAjax;
function bigMap(_latitude, _longitude, element, useAjax) {
    isAjax = useAjax
    if (document.getElementById(element) != null) {
        var urlToPHP;
        if (useAjax == true) {
            //urlToPHP = "/Content/assets/external/ajax.markers.php";
            urlToPHP = "/Content/assets/external/ajax.markers.js";
        }
        else {
            //urlToPHP = "/Content/assets/external/locations.php";
            urlToPHP = "/Content/assets/external/location.js";
        }
        map = new google.maps.Map(document.getElementById(element), {
            zoom: 9,
            scrollwheel: true,
            center: new google.maps.LatLng(_latitude, _longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: mapStyles
        });

        // Place marker after map is loaded and ready ------------------------------------------------------------------

        google.maps.event.addListenerOnce(map, 'idle', function () {
            $.ajax({
                url: urlToPHP,
                dataType: "script",
                method: "GET",
                success: function (locations) {
                    //Added by kwing for testing
                    var data = eval(locations)
                    //placeMarkers(data);

                    //Original
                    //placeMarkers(locations);
                },
                error: function () {
                    console.log("error");
                }
            });
        });

        // Autocomplete

        autoComplete(map);
        clusterMarkers();
    }
    else {
        console.log("No map element");
    }
}

// Create and place markers function
// Move this functions to be accessible by other scripts
var i;
var a;
var newMarkers = [];
var geocoder;
function placeMarkers(locations) {
    cheater = 0;
    geocoder = new google.maps.Geocoder();
    for (i = 0; i < locations.length; i++) {
        var marker;
        var markerContent = document.createElement('div');
        markerContent.innerHTML =
            '<div class="map-marker">' +
                '<div class="icon">' +
                    '<img src="/Content/assets/img/marker.png">' +
                '</div>' +
            '</div>';

        // Latitude, Longitude and Address
        if (locations[i]["latitude"] && locations[i]["longitude"] && locations[i]["address"]) {
            marker = new RichMarker({
                position: new google.maps.LatLng(locations[i]["latitude"], locations[i]["longitude"]),
                map: map,
                draggable: false,
                content: markerContent,
                flat: true
            });
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    var _this = this;
                    if (useAjax == true) {
                        var id = locations[i]["id"];
                        //ajaxLoadInfobox(i, marker, newMarkers, locations, _this, id);
                    }
                    else {
                        //openInfobox(i, marker, newMarkers, locations, _this);
                    }
                }
            })(marker, i));
        }
            // Only Address
        else if (locations[i]["address"] && locations[i]["latitude"] == undefined && locations[i]["longitude"] == undefined) {
            a = i;
            disp = locations[i]["dispensaryName"];
            geocoder.geocode({ 'address': locations[i]["address"] }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results[0].geometry.location, locations);
                    if (cheater == locations.length) {
                        //Set Zoom animation here
                        setZoomAnimation();
                    }
                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
            // Only Latitude and Longitude
        else if (locations[i]["latitude"] && locations[i]["longitude"] && locations[i]["address"] == undefined) {
            marker = new RichMarker({
                position: new google.maps.LatLng(locations[i]["latitude"], locations[i]["longitude"]),
                map: map,
                draggable: false,
                content: markerContent,
                flat: true
            });
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    var _this = this;
                    if (useAjax == true) {
                        var id = locations[i]["id"];
                        ajaxLoadInfobox(i, marker, newMarkers, locations, _this, id);
                    }
                    else {
                        openInfobox(i, marker, newMarkers, locations, _this);
                    }
                }
            })(marker, i));
        }
            // No coordinates
        else {
            console.log("No location coordinates");
        }

        //newMarkers.push(marker);
    }
    //navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
}

function clusterMarkers()
{
    markerCluster = new MarkerClusterer(map, [], { styles: clusterStyles, maxZoom: 10 });
}

function callback(position, locations) {
    var dis;
    disp = locations[cheater]["dispensaryName"];
    var markerLatLng = new google.maps.LatLng(position.lat(), position.lng());
    var markerDistance = google.maps.geometry.spherical.computeDistanceBetween(_currentLatLng, markerLatLng);
    
    if (markerDistance < _nearestDistance || _nearestDistance == 0) {
        _nearestDistance = markerDistance;
        _nearestMarkerLatLng = markerLatLng;
        map.setZoom(3);
        map.setCenter(markerLatLng);
    }

    var markerContent1 = document.createElement('div');
    markerContent1.innerHTML =
        '<div class="map-marker">' +
            '<div class="icon">' +
                '<img id="marker" src="/Content/assets/img/marker.png">' +
            '</div>' +
        '</div>';

    var marker2 = new RichMarker({
        position: position,
        map: map,
        draggable: false,
        content: markerContent1,
        flat: true,
        url: '/Dispensary/Detail/' + disp,
        title: cheater
    });

    google.maps.event.addListener(marker2, 'click', (function (marker, a) {
        return function () {
            var _this = this;
            if (isAjax == true) {
                window.open(_this.url, '_blank')
                //ajaxLoadInfobox(a, marker, newMarkers, locations, _this, id);
            }
            else {
                //openInfobox(a, marker, newMarkers, locations, _this);
            }
        }
    })(marker2, a));

    //marker2.addListener('click', function () {
    //    var position = marker2.getPosition();
    //    var markerLocation = new google.maps.LatLng(position.lat(), position.lng());
    //    drawDrivingDirection(markerLocation);
    //});

    marker2.addListener('mouseover', (function (marker, a) {
        return function () {
            var _this = this;
            if (isAjax == true) {
                openInfobox(_this.title, _this.url, newMarkers, locations, _this, true);
                //infowindow.setContent(_this.title);
                //infowindow.open(map, marker2);
            }
        }
    })(marker2, a));

    newMarkers.push(marker2);
    markerCluster.addMarker(marker2);
    cnt = map.getZoom();
    cheater++;
}
// Ajax loading of infobox -------------------------------------------------------------------------------------

function ajaxLoadInfobox(i, marker, newMarkers, locations, _this, id) {
    $.ajax({
        url: "/Content/assets/external/ajax.infobox.html",
        dataType: "html",
        data: { id: id },
        method: "GET",
        success: function (htmlContent) {
            openInfobox(i, marker, newMarkers, locations, _this, htmlContent);
        },
        error: function () {
            console.log("error");
        }
    });
}

// Infobox -----------------------------------------------------------------------------------------------------

var lastInfobox;

function openInfobox(i, url, newMarkers, locations, _this, isMouseover) {
    if (isMouseover) {
        if (isAjax == true) {
            var boxText = document.createElement("div");
            boxText.innerHTML =
                '<a href="/Dispensary/Detail/' + locations[i]["dispensaryName"] + '" class="infobox-inner" target="_blank">' +
                    '<div class="image-wrapper">' +
                        '<div class="label-wrapper">' +
                            '<figure class="label label-info">Dispensary Information</figure>' +
                        '</div>' +
                        '<div class="wrapper">' +
                        '</div>' +
                        '<div class="image" style="background-image: url(' + locations[i]["avatarPath"] + ')"></div>' +
                    '</div>' +
                    '<div class="meta" style="text-align:center; width: 220px;height: 80px;position: relative;">' +
                        '<span>' + locations[i]["dispensaryName"] + '</span>' +
                        '<div class="info">' +
                                '<span style="text-align:center;">' + locations[i]["address"] + '</span>' +
                            '</div>' +
                    '</div>' +
                '</a>';
            ////console.log(useAjax);
            //boxText = htmlContent;
        }

        infoboxOptions = {
            content: boxText,
            disableAutoPan: false,
            pixelOffset: new google.maps.Size(-16, -50),
            zIndex: null,
            alignBottom: true,
            boxClass: "infobox-wrapper",
            enableEventPropagation: true,
            closeBoxMargin: "0px 0px -8px 0px",
            closeBoxURL: "/Content/assets/img/close-btn.png",
            infoBoxClearance: new google.maps.Size(1, 1)
        };

        if (lastInfobox != undefined) {
            lastInfobox.close();
        }

        newMarkers[i].infobox = new InfoBox(infoboxOptions);
        newMarkers[i].infobox.open(map, _this);
        lastInfobox = newMarkers[i].infobox;
    }
    else {
        if (lastInfobox != undefined) {
            lastInfobox.close();
        }
    }
}

// Geo Location ------------------------------------------------------------------------------------------------

function success(position) {
    var center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    map.panTo(center);
    $('#map').removeClass('fade-map');
}

// Geo Location on button click --------------------------------------------------------------------------------

$('.geo-location').on("click", function () {
    if (navigator.geolocation) {
        $('#map').addClass('fade-map');
        navigator.geolocation.getCurrentPosition(success);
    } else {
        error('Geo Location is not supported');
    }
});

function removeMarkers() {
    for (i = 0; i < newMarkers.length; i++) {
        if (typeof newMarkers[i] != 'undefined') {
            newMarkers[i].setMap(null);
        }
    }
    markers = [];
    markerCluster.clearMarkers();
}

function setZoomAnimation() {
    var max = 18;
    if (cnt >= max) {
        return;
    }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function (event) {
            google.maps.event.removeListener(z);
            cnt++;
            setZoomAnimation();
        });
        setTimeout(function () { map.setZoom(cnt) }, 180); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
}

//var direction;
//function drawDrivingDirection(markerLocation)
//{
//    if (direction != undefined) {
//        direction.setMap(null);
//    }

//    var currentLocation = new google.maps.LatLng(_latitude, _longitude);
//    var directionsService = new google.maps.DirectionsService();
//    var request = {
//        origin: currentLocation,
//        destination: markerLocation,
//        travelMode: google.maps.DirectionsTravelMode.DRIVING
//    };

//    directionsService.route(request, function (response, status) {
//        if (status == google.maps.DirectionsStatus.OK) {
//            var directionsDisplay = new google.maps.DirectionsRenderer();
//            directionsDisplay.setMap(map);
//            directionsDisplay.setOptions({ suppressMarkers: true });
//            directionsDisplay.setDirections(response);
//            direction = directionsDisplay;
//        }
//    });
//}