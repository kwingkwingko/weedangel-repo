﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeedAngel.Utilities
{
    public static class Constants
    {
        public const string Dispensary_Images_Path = "~/Uploads/{0}/Images";
        public const string Dispensary_Videos_Path = "~/Uploads/{0}/Videos";
        public const string Dispensary_Profile_Path = "~/Uploads/{0}/Profile";
        public const string Product_Img_Path = "~/Uploads/Products/{0}";
        public const string User_Profile_Path = "~/Uploads/{0}/UserProfile";
        public const string Gender_Male = "Male";
        public const string Gender_Female = "Female";
        public const string Username_NotExists = "Username does not exist";
        public const string Reset_Successful = "Password reset is successful. <br /> Please check your email.";
        public const string User_Does_Not_Exists = "User does not exists.";
        
        #region -- Email Constants -- 
        public const string Forgot_Pass_Subject = "Weed Angel: Forgot Password";
        public const string Verify_Email_Subject = "Weed Angel: Verify your email address";
        public const string Contact_Us_Subject = "Weed Angel: Customer Inquiry";
        #endregion

        #region -- Login Message Code / Messages --
        public const string UnAuthorized = "unauthorized";
        public const string Verified = "verified";

        public const string UnAuthorized_Str = "You are not authorized to perform current operation!";
        public const string Verified_Str = "Successfully verified email.";
        public const string Login_Error_Invalid = "Username/password is incorrect.";
        public const string Login_Error_NotVerified = "Please verify your email first before logging-in.";
        #endregion
    }
}