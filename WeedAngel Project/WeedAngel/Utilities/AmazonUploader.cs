﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace WeedAngel.Utilities
{
    public static class AmazonHandler
    {
        public static bool UploadFileToAws(string bucketName, string subDirectory, string foldername, string filename, Stream inputStream)
        {
            var accessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            var secretKey = ConfigurationManager.AppSettings["AWSSecretKey"];

            using (AmazonS3Client s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.USWest1))
            {
                using (TransferUtility utility = new TransferUtility(s3Client))
                {
                    try
                    {
                        var key = (!String.IsNullOrEmpty(subDirectory)) ? string.Format(@"{0}/{1}/{2}", subDirectory, foldername, filename) : 
                                                                          string.Format(@"{0}/{1}", foldername, filename);

                        TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
                        request.BucketName = bucketName;
                        request.InputStream = inputStream;
                        request.CannedACL = S3CannedACL.PublicReadWrite;
                        if (!String.IsNullOrEmpty(key)) // Optional index
                            request.Key = key;
                        utility.Upload(request);
                    }
                    catch (Exception ex)
                    { }

                    return true;
                }
            }
        }

        public static List<String> GetBucketKeyFiles(string bucketName, string subDirectory, string uniqueKey, string foldername)
        {
            var images = new List<string>();
            var accessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
            var secretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();
            var bucketPath = (subDirectory.Equals("dispensary")) ? ConfigurationManager.AppSettings["AWSDispensaryBucketPath"].ToString() :
                                        ConfigurationManager.AppSettings["AWSProductBucketPath"].ToString();

            // Create a client
            using (AmazonS3Client s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.USWest1))
            {
                var prefix = string.Format(@"{0}/{1}/{2}/", subDirectory, uniqueKey, foldername);

                // List all objects
                ListObjectsRequest listRequest = new ListObjectsRequest
                {
                    BucketName = bucketName,
                    Prefix = prefix,
                    Delimiter = "/"
                };

                ListObjectsResponse listResponse;
                do
                {
                    // Get a list of objects
                    listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object obj in listResponse.S3Objects)
                    {
                        var rootBucketPath = bucketPath.Replace(subDirectory + "/", string.Empty);
                        var imagePath = rootBucketPath + obj.Key;
                        images.Add(imagePath);
                    }

                    // Set the marker property
                    listRequest.Marker = listResponse.NextMarker;
                } while (listResponse.IsTruncated);
            }
            return images;
        }

        public static void DeleteObject(string bucketName, string subDirectory, string uniqueKey, string foldername, string filename, string bucketPath)
        {
            var accessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
            var secretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();

            var key = (!String.IsNullOrEmpty(uniqueKey)) ? string.Format(@"{0}/{1}/{2}/{3}", subDirectory, uniqueKey, foldername, filename):
                                                           string.Format(@"{0}/{1}/{2}", subDirectory, foldername, filename);
            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = bucketName,
                Key = key
            };

            using (AmazonS3Client s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.USWest1))
            {
                s3Client.DeleteObject(deleteObjectRequest);
            }
        }
    }
}