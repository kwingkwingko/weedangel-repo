﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeedAngel.Repositories;

namespace WeedAngel.Utilities
{
    public  class Dropdown
    {
        private readonly DropdownRepository _dropdownRepository;

        public Dropdown()
        {
            string _connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            _dropdownRepository = new DropdownRepository(_connStr);
        }

        public List<SelectListItem> GenerateProdTypeDDL()
        {
            return _dropdownRepository.GetProductTypes();
        }

        public List<SelectListItem> GenerateMaritalDDL()
        {
            List<SelectListItem> maritalList = new List<SelectListItem>();
            maritalList.Add(new SelectListItem { Value = "1", Text = "Single" });
            maritalList.Add(new SelectListItem { Value = "2", Text = "Married" });
            maritalList.Add(new SelectListItem { Value = "3", Text = "Divorce" });
            maritalList.Add(new SelectListItem { Value = "4", Text = "Complicated" });

            return maritalList;
        }
    }
}