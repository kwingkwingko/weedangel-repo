﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WeedAngel.Utilities
{
    public static class Helper
    {
        public static string GetStandardTimeFormat(string time)
        {
            if (!String.IsNullOrEmpty(time))
            {
                return DateTime.Parse(time).ToString(@"hh\:mm tt");
            }

            return string.Empty;
        }

        public static void CreateFolderLocation(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void DeleteFolderContents(string path) 
        {
            Array.ForEach(Directory.GetFiles(path), File.Delete);
        }

        public static List<string> GetAllFileNames(string path)
        { 
            if (!String.IsNullOrEmpty(path))
            {
                try
                {
                    string[] directories = Directory.GetFiles(path, "*.*")
                                          .Select(x => Path.GetFileName(x))
                                          .ToArray();
                    return directories.ToList();
                }
                catch (Exception ex){ }

                return null;

            }
            return null;
        }

        public static string GetFilenameFromURI(string url)
        {
            var filename = string.Empty;
            Uri uri = new Uri(url);

            if (uri.IsAbsoluteUri)
                filename = Path.GetFileName(uri.AbsolutePath);
            return filename;
        }

        public static string TrimFrontIfLongerThan(string value, int maxLength)
        {
            if (value.Length > maxLength)
            {
                return value.Substring(0, maxLength) + "...";
            }

            return value;
        }
    }
}