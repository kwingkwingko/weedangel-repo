﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WeedAngel.Repositories
{
    public class DataAccessLayer
    {
        private string m_ConnectionString {get; set; }
        public string m_PublicConnection { get; set; }
        private string ConnectionString
        {
            get
            {
                if (string.IsNullOrWhiteSpace(m_ConnectionString))
                {
                    m_ConnectionString = m_PublicConnection;
                }
                return m_ConnectionString;
            }
            set
            {
                m_ConnectionString = value;
            }
        }

        public DataAccessLayer()
        {
        }

        public DataAccessLayer(string connectionstring)
        {
            m_PublicConnection = connectionstring;
        }

      
        #region DataReader
        protected internal T GetEntity<T>(string storedProcedureName, List<SqlParameter> parameters)
        {
            Type t = typeof(T);
            T objReturn = Activator.CreateInstance<T>();
            IDataReader dr = default(IDataReader);
            try
            {

                string connectionString = ConnectionString;
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Close();
                    conn.Open();
                    var cmd = conn.CreateCommand();
                    cmd.Parameters.Clear();
                    cmd.CommandText = storedProcedureName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (var parameter in parameters)
                    {
                        cmd.Parameters.Add(parameter);
                    }
                    IDbCommand command = cmd;
                    IDataReader reader = command.ExecuteReader();

                    objReturn = FillObject<T>(reader);
                }
            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                // ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            }
            finally
            {


            }
            return objReturn;
        }

        protected internal List<T> GetEntityList<T>(string storedProcedureName, List<SqlParameter> parameters)
        {
            List<T> list = new List<T>();
            try
            {
                string connectionString = ConnectionString;
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Close();
                    conn.Open();
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = storedProcedureName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (var parameter in parameters)
                    {
                        cmd.Parameters.Add(parameter);
                    }
                    IDbCommand command = cmd;
                    IDataReader reader = command.ExecuteReader();

                    list = FillCollection<T>(reader);
                }
            }
            catch (Exception)
            { throw; }
            finally { }
            return list;
        }

        protected internal T GetEntity<T>(string storedProcedureName, Object entity)
        {
            Type t = typeof(T);
            T objReturn = Activator.CreateInstance<T>();
            IDataReader dr = default(IDataReader);
            try
            {

                string connectionString = ConnectionString;
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Close();
                    conn.Open();
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = storedProcedureName;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (entity != null)
                    {
                        SqlCommandBuilder.DeriveParameters(cmd);
                        PropertyInfo entitymember = default(PropertyInfo);
                        foreach (SqlParameter _param in cmd.Parameters)
                        {
                            if (_param.Direction == ParameterDirection.Input)
                            {
                                entitymember = entity.GetType().GetProperty(_param.ParameterName.Replace("@", ""));
                                String _paramvalue = entitymember.GetValue(entity, null).ToString();
                                _param.Value = (string.IsNullOrEmpty(_paramvalue) || _paramvalue == string.Empty ? null : _paramvalue);
                            }
                        }
                    }
                    IDbCommand command = cmd;
                    IDataReader reader = command.ExecuteReader();

                    objReturn = FillObject<T>(reader);
                }
            }
            catch (Exception ex)
            {
                //Log exception in Exception Logger.
                // ex.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            }
            finally
            {


            }
            return objReturn;
        }

        protected internal List<T> GetEntityList<T>(string storedProcedureName, Object entity)
        {
            List<T> list = new List<T>();
            try
            {
                string connectionString = ConnectionString;
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Close();
                    conn.Open();
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = storedProcedureName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (entity != null)
                    {
                        SqlCommandBuilder.DeriveParameters(cmd);
                        PropertyInfo entitymember = default(PropertyInfo);
                        foreach (SqlParameter _param in cmd.Parameters)
                        {
                            if (_param.Direction == ParameterDirection.Input)
                            {
                                entitymember = entity.GetType().GetProperty(_param.ParameterName.Replace("@", ""));
                                String _paramvalue = entitymember.GetValue(entity, null).ToString();
                                _param.Value = (string.IsNullOrEmpty(_paramvalue) || _paramvalue == string.Empty ? null : _paramvalue);
                            }
                        }
                    }
                    IDbCommand command = cmd;
                    IDataReader reader = command.ExecuteReader();

                    list = FillCollection<T>(reader);
                }
            }
            catch (Exception)
            { throw; }
            finally { }
            return list;
        }

        private static List<T> FillCollection<T>(IDataReader dr)
        {
            List<T> objFillCollection = new List<T>();
            T objFillObject;

            while (dr.Read())
            {
                objFillObject = dr.ConvertedEntity<T>();
                objFillCollection.Add(objFillObject);
            }

            if (!(dr == null))
            {
                dr.Close();
            }
            return objFillCollection;
        }
        private static T FillObject<T>(IDataReader dr)
        {
            T objFillObject = Activator.CreateInstance<T>();
            while (dr.Read())
            {
                objFillObject = dr.ConvertedEntity<T>();
            }

            if (!(dr == null))
            {
                dr.Close();
            }
            return objFillObject;
        }
        #endregion

        #region dataset datatable
        protected internal DataSet Get_DataSet(String spname, Object entity)
        {
            SqlDataAdapter sda = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds.RemotingFormat = SerializationFormat.Binary;
            String conString = String.Empty;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(spname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (entity != null)
                {
                    SqlCommandBuilder.DeriveParameters(cmd);
                    PropertyInfo entitymember = default(PropertyInfo);
                    foreach (SqlParameter _param in cmd.Parameters)
                    {
                        if (_param.Direction == ParameterDirection.Input)
                        {
                            entitymember = entity.GetType().GetProperty(_param.ParameterName.Replace("@", ""));
                            String _paramvalue = entitymember.GetValue(entity, null).ToString();
                            _param.Value = (string.IsNullOrEmpty(_paramvalue) || _paramvalue == string.Empty ? null : _paramvalue);
                        }
                    }
                }
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                entity = null;
                cmd = null;
                sda = null;
                con.Close();
            }
            return ds;
        }
        protected internal DataTable Get_DataTable(String spname, Object entity)
        {
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable();
            dt.RemotingFormat = SerializationFormat.Binary;
            String conString = String.Empty;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(spname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (entity != null)
                {
                    SqlCommandBuilder.DeriveParameters(cmd);
                    PropertyInfo entitymember = default(PropertyInfo);
                    foreach (SqlParameter _param in cmd.Parameters)
                    {
                        if (_param.Direction == ParameterDirection.Input)
                        {
                            entitymember = entity.GetType().GetProperty(_param.ParameterName.Replace("@", ""));
                            var entityValue = entitymember.GetValue(entity, null);
                            String _paramvalue = entityValue != null ? entityValue.ToString() : null;
                            _param.Value = (string.IsNullOrEmpty(_paramvalue) || _paramvalue == string.Empty ? null : _paramvalue);
                        }
                    }
                    sda.SelectCommand = cmd;
                    sda.Fill(dt);
                    entity = null;
                    cmd = null;
                    sda.Dispose(); sda = null;
                    con.Close();
                }
            }
            return dt;
        }
        protected internal DataTable Get_DataTable(String spname)
        {
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable();
            dt.RemotingFormat = SerializationFormat.Binary;
            String conString = String.Empty;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(spname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                
                SqlCommandBuilder.DeriveParameters(cmd);
                PropertyInfo entitymember = default(PropertyInfo);
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                cmd = null;
                sda.Dispose(); sda = null;
                con.Close();
            }
            return dt;
        }
        #endregion

        #region "string" "int"
        protected internal int GetSingleValue_Int(String spname, Object entity)
        {
            Object result = new Object();
            String conString = String.Empty;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(spname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (entity != null)
                {
                    SqlCommandBuilder.DeriveParameters(cmd);
                    PropertyInfo entitymember = default(PropertyInfo);
                    foreach (SqlParameter _param in cmd.Parameters)
                    {
                        if (_param.Direction == ParameterDirection.Input)
                        {
                            entitymember = entity.GetType().GetProperty(_param.ParameterName.Replace("@", ""));
                            String _paramvalue = entitymember.GetValue(entity, null).ToString();
                            _param.Value = (string.IsNullOrEmpty(_paramvalue) || _paramvalue == string.Empty ? null : _paramvalue);
                        }
                    }
                }
                result = cmd.ExecuteScalar();
                cmd.Connection.Close();
                entity = null;
                cmd = null;
            }
            if (result == null)
                result = 0;
            return Convert.ToInt32(result);
        }
        protected internal string GetSingleValue_String(String spname, Object entity)
        {
            Object res = new Object();
            String conString = String.Empty;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(spname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (entity != null)
                {
                    SqlCommandBuilder.DeriveParameters(cmd);
                    PropertyInfo entitymember = default(PropertyInfo);
                    foreach (SqlParameter _param in cmd.Parameters)
                    {
                        if (_param.Direction == ParameterDirection.Input)
                        {
                            entitymember = entity.GetType().GetProperty(_param.ParameterName.Replace("@", ""));
                            var entityValue = entitymember.GetValue(entity, null);
                            String _paramvalue = entityValue != null ? entityValue.ToString() : null; 
                            _param.Value = (string.IsNullOrEmpty(_paramvalue) || _paramvalue == string.Empty ? null : _paramvalue);
                        }
                    }
                }
                res = cmd.ExecuteScalar();
                cmd.Connection.Close();
                entity = null;
                cmd = null;
                if(res==null)
                    res = "";
                else if (String.IsNullOrEmpty(res.ToString()))
                    res = "";
                return res.ToString();
            }
        }
        #endregion
    }
}