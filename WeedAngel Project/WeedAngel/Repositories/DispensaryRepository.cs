﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WeedAngel.Contracts;
using WeedAngel.Models;

namespace WeedAngel.Repositories
{
    public class DispensaryRepository : IDispensaryRepository
    {
        private readonly string _connectionString;
        private DataAccessLayer dataAccessLayer;

        public DispensaryRepository(string connectionString)
        {
            _connectionString = connectionString;
            dataAccessLayer = new DataAccessLayer(_connectionString);
        }

        public DataTable GetDispensary(DispensaryFilterModel filters)
        {
            DataTable dtDispensary = dataAccessLayer.Get_DataTable("dbo.GetDispensaries", filters);
            return dtDispensary;
        }

        public DataTable GetTopDispensaries()
        { 
            DataTable dtDispensary = dataAccessLayer.Get_DataTable("dbo.GetTopDispensaries", new {  });
            return dtDispensary;
        }

        public DispensaryModel GetDispensaryById(int dispensaryId)
        {
            List<SqlParameter> parameter = new List<SqlParameter>()
            {
                new SqlParameter("@DispensaryId", dispensaryId)
            };

            var dispensaryModel = dataAccessLayer.GetEntity<DispensaryModel>("dbo.GetDispensaryById", parameter);
            return dispensaryModel ?? null;
        }

        public DispensaryModel GetDispensaryByName(string dispensaryName)
        {
            List<SqlParameter> parameter = new List<SqlParameter>()
            {
                new SqlParameter("@DispensaryName", dispensaryName)
            };

            var dispensaryModel = dataAccessLayer.GetEntity<DispensaryModel>("dbo.GetDispensaryByName", parameter);
            return dispensaryModel ?? null;
        }

        public DataTable GetDispensariesByZipCode(string zipCode)
        {
            var data = dataAccessLayer.Get_DataTable("dbo.GetDispensaryByZipCode", new { ZipCode = zipCode });
            return data ?? null;
        }

        public void CreateDispensary(DispensaryModel dispensaryModel)
        {
            dataAccessLayer.GetSingleValue_String("dbo.CreateDispensary", dispensaryModel);
        }

        public void UpdateDispensary(DispensaryModel dispensaryModel)
        {
            dataAccessLayer.GetSingleValue_String("dbo.UpdateDispensary", dispensaryModel);
        }

        public void DeleteDispensary(int DispensaryId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                using (var cmd = new SqlCommand("dbo.DeleteDispensary"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@DispensaryId", SqlDbType.Int).Value = DispensaryId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}