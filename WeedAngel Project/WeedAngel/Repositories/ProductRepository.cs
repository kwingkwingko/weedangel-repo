﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WeedAngel.Models;

namespace WeedAngel.Repositories
{
    public class ProductRepository
    {
        private readonly string _connectionString;
        private DataAccessLayer dataAccessLayer;

        public ProductRepository(string connectionstring)
        {
            _connectionString = connectionstring;
            dataAccessLayer = new DataAccessLayer(_connectionString);
        }

        public DataTable GetProducts(int dispensaryId, string productName)
        {
            DataTable dtProductList = dataAccessLayer.Get_DataTable("dbo.GetProductsByDispensary", new { DispensaryId = dispensaryId, ProductName = productName });
            return dtProductList;
        }

        public ProductModel GetProductById(int productId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            SqlParameter param = new SqlParameter("@ProductId", productId);
            parameters.Add(param);

            var productModel = dataAccessLayer.GetEntity<ProductModel>("dbo.GetProductById", parameters);
            return productModel ?? null;
        }

        public void CreateProduct(ProductModel product)
        {
            dataAccessLayer.GetSingleValue_String("dbo.AddProduct", product);
        }

        public void UpdateProduct(ProductModel product)
        {
            dataAccessLayer.GetSingleValue_String("dbo.UpdateProduct", product);
        }

        public void DeleteProduct(int productId)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                using (var cmd = new SqlCommand("dbo.DeleteProduct"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = productId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}