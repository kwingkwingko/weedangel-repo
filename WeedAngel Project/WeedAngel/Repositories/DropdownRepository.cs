﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeedAngel.Repositories
{
    public class DropdownRepository
    {
        private readonly string _connectionString;
        private DataAccessLayer dataAccessLayer;

        public DropdownRepository( string connectionString)
        {
            _connectionString = connectionString;
            dataAccessLayer = new DataAccessLayer(_connectionString);
        }

        public List<SelectListItem> GetProductTypes()
        {
            DataTable productDt = dataAccessLayer.Get_DataTable("dbo.GetProductTypes");
            var list = productDt.AsEnumerable().Select(row =>
                new SelectListItem
                {
                    Text = row.Field<string>("ProductTypeDesc"), 
                    Value = row.Field<int>("ProductTypeId").ToString()
                }).ToList();
            return list ?? null;
        }

        public List<SelectListItem> GetTimeZone()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            ReadOnlyCollection<TimeZoneInfo> tz = TimeZoneInfo.GetSystemTimeZones();

            foreach (var timeZone in tz)
            {
                list.Add(new SelectListItem { Value = timeZone.Id, Text = timeZone.DisplayName });
            }

            return list;
        }
    }
}