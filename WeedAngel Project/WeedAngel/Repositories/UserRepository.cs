﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WeedAngel.Models;

namespace WeedAngel.Repositories
{
    public class UserRepository
    {
        private readonly string _connectionString;
        private DataAccessLayer dataAccessLayer;

        public UserRepository(string connectionstring)
        {
            _connectionString = connectionstring;
            dataAccessLayer = new DataAccessLayer(_connectionString);         
        }

        public UserModel GetUserById(int userId)
        {
            List<SqlParameter> parameter = new List<SqlParameter>()
            {
                new SqlParameter("@UserId", userId)
            };

            var userModel = dataAccessLayer.GetEntity<UserModel>("dbo.GetUserById", parameter);
            return userModel ?? null;
        }

        public UserModel GetUserByUsername(string userName)
        {
            List<SqlParameter> parameter = new List<SqlParameter>()
            {
                new SqlParameter("@Username", userName)
            };

            var userModel = dataAccessLayer.GetEntity<UserModel>("dbo.GetUserByUsername", parameter);
            return userModel ?? null;
        }

        public DataSet ValidateUser(LogOnModel model)
        {
            if (model != null)
                return dataAccessLayer.Get_DataSet("dbo.ValidateUser", model);
            return null;
        }

        public int CreateUser(RegisterModel model)
        {
            if (model != null)
                return dataAccessLayer.GetSingleValue_Int("dbo.CreateUser", model);
            return 0;
        }

        public void UpdateUser(UserModel model)
        {
            dataAccessLayer.GetSingleValue_String("dbo.UpdateUser", model);
        }

        public int UpdatePassword(PasswordModel model)
        {
            return dataAccessLayer.GetSingleValue_Int("dbo.UpdatePassword", model);
        }

        public int ResetPassword(string username, string tempPassword)
        {
            var data = new { Username = username, TempPassword = tempPassword };
            return dataAccessLayer.GetSingleValue_Int("dbo.ResetPassword", data);
        }

        public int VerifyUser(string username)
        {
            var data = new { Username = username };
            return dataAccessLayer.GetSingleValue_Int("dbo.VerifyUser", data);
        }
    }
}