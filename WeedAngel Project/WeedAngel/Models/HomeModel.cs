﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeedAngel.Models
{
    public class HomeModel
    {
        public List<DispensaryZipModel> topDispensaries { get; set; }
        public string MessageCode { get; set; }
        public string Message { get; set; }
    }
}