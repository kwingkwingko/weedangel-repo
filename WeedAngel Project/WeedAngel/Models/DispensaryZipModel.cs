﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WeedAngel.Models
{
    public class DispensaryZipModel
    {
        public int id { get; set; }
        public string address { get; set; }
        public string dispensaryName { get; set; }
        public string dispensaryDesc { get; set; }
        public string avatarPath { get; set; }
        public string street { get; set; }
    }
}