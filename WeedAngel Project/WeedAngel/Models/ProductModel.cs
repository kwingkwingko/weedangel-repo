﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeedAngel.Models
{
    public class ProductModel
    {
        public int ProductId { get; set; }

        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [DisplayName("Product Category")]
        public int? CategoryTypeId { get; set; }

        public string ProductCategoryName{ get; set; }

        [DisplayName("GRAM")]
        public double? Price1 { get; set; }

        [DisplayName("2 GRAMS")]
        public double? Price2 { get; set; }

        [DisplayName("1/8")]
        public double? Price3 { get; set; }

        [DisplayName("1/4")]
        public double? Price4 { get; set; }

        [DisplayName("1/2")]
        public double? Price5 { get; set; }

        [DisplayName("Each")]
        public double? Each { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        public string ImagePath { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public int ProductDispensaryId { get; set; }

        public List<SelectListItem> productTypes { get; set; }
    }
}