﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeedAngel.Models
{
    public class DispensaryModel
    {
        public int DispensaryId { get; set; }

        public int ? CreatedBy { get; set; }

        [DisplayName("Dispensary Name")]
        public string DispensaryName { get; set; }

        [DisplayName("Dispensary Unique Name")]
        public string DispensaryUniqueName {get;set;}

        [DisplayName("Street")]
        public string Street { get; set; }

        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("State")]
        public string State { get; set; }

        [DisplayName("Country")]
        public string Country { get; set; }

        [DisplayName("Zipcode")]
        public string Zipcode { get; set; }

        [DisplayName("Region")]
        public string RegionId { get; set; }

        [DisplayName("Timezone")]
        public string TimeZoneId { get; set; }

        [DisplayName("Phone No.")]
        public string PhoneNo { get; set; }

        [DisplayName("Website")]
        public string Website { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Avatar Name")]
        public string AvatarName { get; set; }

        [DisplayName("Avatar Path")]
        public string AvatarPath { get; set; }

        [DisplayName("Facebook")]
        public string Facebook { get; set; }

        [DisplayName("Twitter")]
        public string Twitter { get; set; }

        [DisplayName("Instagram")]
        public string Instagram { get; set; }

        [DisplayName("Intro Body")]
        public string IntroBody { get; set; }

        [DisplayName("Body")]
        public string Body { get; set; }

        [DisplayName("Sunday Open")]
        public string SunDayOpen { get;set; }

        [DisplayName("Sunday Close")]
        public string SunDayClose { get; set; }

        [DisplayName("Monday Open")]
        public string MonDayOpen { get; set; }

        [DisplayName("Monday Close")]
        public string MonDayClose { get; set; }

        [DisplayName("Tuesday Open")]
        public string TuesDayOpen { get; set; }

        [DisplayName("Tuesday Close")]
        public string TuesDayClose { get; set; }

        [DisplayName("Wednesday Open")]
        public string WednesDayOpen { get; set; }

        [DisplayName("Wednesday Close")]
        public string WednesDayClose { get; set; }

        [DisplayName("Thursday Open")]
        public string ThursDayOpen { get; set; }

        [DisplayName("Thursday Close")]
        public string ThursDayClose { get; set; }

        [DisplayName("Friday Open")]
        public string FriDayOpen { get; set; }

        [DisplayName("Friday Close")]
        public string FriDayClose { get; set; }

        [DisplayName("Saturday Open")]
        public string SaturDayOpen { get; set; }

        [DisplayName("Saturday Close")]
        public string SaturDayClose { get; set; }

        [DisplayName("Latitude")]
        public string Latitude { get; set; }

        [DisplayName("Longitude")]
        public string Longitude { get; set; }

        [DisplayName("Address")]
        public string Address { get; set; }

        [DisplayName("Handicap")]
        public bool IsHandicap { get;set; }

        [DisplayName("Security Guard")]
        public bool IsSecurityGuard { get; set; }

        [DisplayName("Lounge")]
        public bool IsLounge{ get; set; }

        [DisplayName("Credit Cards")]
        public bool IsCreditCards { get; set; }

        [DisplayName("Is 18?")]
        public bool Is18 { get; set; }

        [DisplayName("Is 21?")]
        public bool Is21 { get; set; }

        [DisplayName("Pictures")]
        public bool IsPictures { get; set; }

        [DisplayName("Videos")]
        public bool IsVideos { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Date Created")]
        public DateTime CreatedDateTime{ get; set; }

        public List<ProductModel> products { get; set; }

        public List<string> ImageSrcList { get; set; }

        public string ExistingImageFiles { get; set; }

        public string ExistingVideoFiles { get; set; }

        public string ImageSources { get; set; }

        public string videoSources { get; set; }

        public Guid UniqueKey { get; set; }

        public bool IsProfileChanged { get; set; }

        public List<ProductListModel> productCategories { get; set; }

        public List<SelectListItem> TimeZoneList { get; set; }

        public string errorText { get; set; }

        public string DispensaryImages { get; set; }

        public string DeletedImages { get; set; }
    }
}