﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeedAngel.Models
{
    public class DispensarySearchModel
    {
        public List<DispensaryResultsModel> DispensaryList { get; set; }
        public string UserId { get; set; }
        public bool IsMyDispensary { get; set; }
    }

    public class DispensaryResultsModel
    {
        public string DispensaryName { get; set; }
        public string Description { get; set; }
        public string Speed { get; set; }
        public string ImageName { get; set; }
        public string ImageDescription { get; set; }
    }

    public class DispensaryFilterModel
    {
        public string DispensaryName { get; set; }
        public int StartLimit { get; set; }
        public int EndLimit { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string UserId { get; set; }
        public bool IsMyDispensary { get; set; }
    }
}