﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeedAngel.Models
{
    public class UserModel
    { 
        public int UserId { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string Surname { get; set; }

        [DisplayName("Profile Image Name")]
        public string ProfileImageName { get; set; }

        [DisplayName("Profile Image Path")]
        public string ProfileImagePath { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        public string Phone { get; set; }

        [DisplayName("Mobile")]
        public string Mobile { get; set; }

        [DisplayName("Gender")]
        public string Gender { get; set; }

        [DisplayName("Marital Status")]
        public string MaritalStatus { get; set; }

        [DisplayName("About me")]
        public string AboutMe { get; set; }

        [DisplayName("State")]
        public string State { get; set; }

        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("Street")]
        public string Street { get; set; }

        [DisplayName("Zip")]
        public string Zipcode { get; set; }

        [DisplayName("Facebook")]
        public string Facebook { get; set; }

        [DisplayName("Twitter")]
        public string Twitter { get; set; }

        [DisplayName("Instagram")]
        public string Instagram { get; set; }

        [DisplayName("Wanna Follow Me")]
        public string WannaFollowMeUrl { get; set; }

        [DisplayName("Favorite Way To Get High")]
        public string FavWayToGetHigh { get; set; }

        public List<SelectListItem> MaritalStatusList { get; set; }

        public Guid UniqueKey { get; set; }

        public bool IsMale { get; set; }

        public bool IsFemale { get; set; }

        public string Password { get; set; }
    }

    public partial class PasswordModel
    {
        [DisplayName("Current Password")]
        public string OldPassword { get; set; }

        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [DisplayName("Confirm New Password")]
        public string ConfirmPassword { get; set; }

        public string UserId { get; set; }
    }
}