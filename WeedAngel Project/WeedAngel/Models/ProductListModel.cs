﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeedAngel.Models
{
    public class ProductListModel
    {
        public string CategoryName { get; set; }
        public List<ProductModel> prodList { get; set; }
        public int CreatedBy { get; set; }
    }
}