﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using WeedAngel.Models;
using WeedAngel.Repositories;
using WeedAngel.Utilities;

namespace WeedAngel.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserRepository _userRepository;
        private EmailHelper _emailHelper;
        public AccountController()
        {
            string _connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            _userRepository = new UserRepository(_connStr);
            _emailHelper = new EmailHelper();
        }

        [HttpPost]
        public JsonResult Login(LogOnModel model)
        {
            var isExists = 0;
            var url = string.Empty;
            var message = string.Empty;
            var userId = string.Empty;
            var lastName = string.Empty;
            var firstName = string.Empty;
            
            var isAuthenticated = false;

            if (ModelState.IsValid)
            {    
                var loginSet = _userRepository.ValidateUser(model);

                if (loginSet != null && loginSet.Tables.Count > 0)
                {
                    isExists = Convert.ToInt32(loginSet.Tables[0].Rows[0]["Result"]);
                }

                if (isExists == 1)
                {
                    firstName = loginSet.Tables[1].Rows[0]["FirstName"].ToString();
                    lastName = loginSet.Tables[1].Rows[0]["Surname"].ToString();
                    userId = loginSet.Tables[1].Rows[0]["UserId"].ToString();

                    var ctx = Request.GetOwinContext();
                    var authenticationManager = ctx.Authentication;

                    var claims = new List<Claim>();
                    claims.Add(new Claim(ClaimTypes.Name, firstName + " " + lastName));
                    claims.Add(new Claim(ClaimTypes.Sid, userId));

                    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                    authenticationManager.SignIn(identity);

                    var claimsPrincipal = new ClaimsPrincipal(identity);
                    Thread.CurrentPrincipal = claimsPrincipal;

                    isAuthenticated = true;
                    // Build redirect URL
                    UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                    string scheme = u.RequestContext.HttpContext.Request.Url.Scheme;
                    url = this.Url.Action("Index", "Home", new { }, scheme);
                }
                else if (isExists == -1)
                {
                    message = Utilities.Constants.Login_Error_NotVerified;
                }
                else
                {
                    message = Utilities.Constants.Login_Error_Invalid;
                }
            }

            return Json(new { isSuccess = isAuthenticated, url = url, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            var isSuccess = false;
            var url = string.Empty;
            UrlHelper urlHelper = new UrlHelper(this.ControllerContext.RequestContext);
            if (ModelState.IsValid)
            {
                model.Password = model.DummyPassword;
                var isCreated = _userRepository.CreateUser(model);

                if (isCreated == 1)
                {
                    isSuccess = true;
                    string scheme = urlHelper.RequestContext.HttpContext.Request.Url.Scheme;
                    var verifyUrl = this.Url.Action("VerifyEmail", "Account", new { username = model.UserName }, scheme);
                    // Send verification email
                    IdentityMessage message = new IdentityMessage
                    {
                        Subject = Utilities.Constants.Verify_Email_Subject,
                        Destination = model.Email,
                        Body = "<p style=\"font:normal 13px Arial;\"> Verify your email address" + "<br>" +
                               "To finish setting up this Weed Angel account.</p><br>" +
                               "<a style=\"font: bold 15px Arial;text-decoration: none;background-color: #488f3e;" +
                                          "color:#fff;padding: 2px 6px 2px 6px;border-top: 1px solid #CCCCCC;cursor:pointer;" +
                                          "border-right: 1px solid #333333;border-bottom: 1px solid #333333;border-left: 1px solid #CCCCCC;\"" +
                               "href=\""+ verifyUrl +"\">"+
                               "<span>Verify "+ model.Email +"</span></a>"
                    };

                    await _emailHelper.SendAsync(message);
                }
                else 
                {
                    isSuccess = false;
                }

                // Rebuild return URL

                url = urlHelper.Action("Index", "Home", null);
            }

            return Json(new { isSuccess = isSuccess, url = url }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VerifyEmail(string username)
        {
            if (!String.IsNullOrEmpty(username))
            {
                var isVerified = _userRepository.VerifyUser(username);

                if (isVerified == 1)
                {
                    return RedirectToAction("Index", "Home", new { message_code = Utilities.Constants.Verified });
                }
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> ForgotPassword(string username)
        {
            var isSuccess = false;
            var retMessage = string.Empty;
            
            if (!String.IsNullOrEmpty(username))
            {
                var tempPassword = System.Web.Security.Membership.GeneratePassword(8, 4);
                var user = _userRepository.GetUserByUsername(username);

                if (user != null && user.UserId > 0)
                {
                    var res = _userRepository.ResetPassword(username, tempPassword);

                    if (res != 0)
                    {
                        IdentityMessage message = new IdentityMessage
                        {
                            Subject = Utilities.Constants.Forgot_Pass_Subject,
                            Destination = user.Email,
                            Body = "<p> You may now log-in to the Weed Angel website using the information below:" + "<br>" + 
                                    "Username: " + username + "<br>" + 
                                    "Password: " + tempPassword + "<br></p>"  
                        };

                        await _emailHelper.SendAsync(message);
                        isSuccess = true;
                        retMessage = Utilities.Constants.Reset_Successful;
                    }
                    else
                    {
                        retMessage = Utilities.Constants.Username_NotExists;
                    }
                }
                else
                {
                    retMessage = Utilities.Constants.User_Does_Not_Exists;
                }
            }

            return Json(new { isSuccess = isSuccess, message = retMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SignOut()
        {
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }
    }
}
