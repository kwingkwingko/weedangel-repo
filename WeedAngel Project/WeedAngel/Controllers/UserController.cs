﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WeedAngel.Contracts;
using WeedAngel.Models;
using WeedAngel.Repositories;
using WeedAngel.Utilities;

namespace WeedAngel.Controllers
{
    public class UserController : Controller
    {
        private readonly UserRepository _userRepository;
        private readonly Dropdown _dropdown;
        public UserController()
        {
            string _connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            _userRepository = new UserRepository(_connStr);
            _dropdown = new Dropdown();
        }

        [Authorize]
        public ActionResult Index()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var userId = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                   .Select(c => c.Value).SingleOrDefault();

            UserModel model = _userRepository.GetUserById(Convert.ToInt32(userId));
            model.MaritalStatusList = _dropdown.GenerateMaritalDDL();
            return View(model);
        }

        [Authorize]
        public ActionResult Edit()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var userId = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                   .Select(c => c.Value).SingleOrDefault();

            UserModel model = _userRepository.GetUserById(Convert.ToInt32(userId));
            model.UserId = Convert.ToInt32(userId);
            model.IsMale = (model.Gender != null && model.Gender.Equals(Constants.Gender_Male)) ? true : false;
            model.IsFemale = (model.Gender != null && model.Gender.Equals(Constants.Gender_Female)) ? true : false;
            model.MaritalStatusList = _dropdown.GenerateMaritalDDL();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserModel model, HttpPostedFileBase image)
        {
            var imageName = string.Empty;
            var userFolderName = "Users";
            var bucketName = ConfigurationManager.AppSettings["AWSUserBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSUserBucketPath"];
            var bucketNameArr = bucketName.Split('/');
            if (ModelState.IsValid)
            {
                Guid guid = model.UniqueKey == Guid.Empty ? Guid.NewGuid() : model.UniqueKey;
                model.UniqueKey = guid;

                if (image != null && image.ContentLength > 0)
                {
                    // Delete profile image in amazon
                    AmazonHandler.DeleteObject(bucketNameArr[0], bucketNameArr[1], model.UniqueKey.ToString(), userFolderName, model.ProfileImageName, bucketPath);

                    // Upload image to amazon
                    imageName = Path.GetFileName(image.FileName);
                    AmazonHandler.UploadFileToAws(bucketName, model.UniqueKey.ToString(), userFolderName, imageName, image.InputStream);

                    model.ProfileImageName = imageName;
                    model.ProfileImagePath = bucketPath + model.UniqueKey.ToString() + "/" + userFolderName + "/" + imageName;
                }
                model.Gender = (model.IsMale) ? Constants.Gender_Male : Constants.Gender_Female;

                _userRepository.UpdateUser(model);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult ChangePassword(PasswordModel model)
        {
            var isSuccess = false;
            int result = -1;

            try
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                var sid = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                       .Select(c => c.Value).SingleOrDefault();

                model.UserId = sid;
                result = _userRepository.UpdatePassword(model);
                isSuccess = true;
            }
            catch (Exception ex)
            { 

            }

            return Json(new { isSuccess = isSuccess, res = result }, JsonRequestBehavior.AllowGet);
        }
    }
}
