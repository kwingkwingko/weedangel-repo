﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using WeedAngel.Models;
using WeedAngel.Repositories;
using WeedAngel.Utilities;

namespace WeedAngel.Controllers
{
    public class HomeController : Controller
    {
        private readonly DispensaryRepository _dispensaryRepository;
        private EmailHelper _emailHelper;

        public HomeController()
        {
            string _connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            _dispensaryRepository = new DispensaryRepository(_connStr);
            _emailHelper = new EmailHelper();
        }

        [OutputCache(Location =OutputCacheLocation.None)]
        public ActionResult Index(string message_code = null)
        {
            HomeModel model = new HomeModel();
            var dtDispensary = _dispensaryRepository.GetTopDispensaries();
            if (dtDispensary != null && dtDispensary.Rows.Count > 0)
            {
                model.topDispensaries = dtDispensary.AsEnumerable()
                                                    .Select(r => new DispensaryZipModel
                                                    {
                                                        id = r.Field<int>("DispensaryId"),
                                                        address = r.Field<string>("Street") + " " + r.Field<string>("City") + " " + r.Field<string>("State"),
                                                        dispensaryName = r.Field<string>("DispensaryName"),
                                                        dispensaryDesc = !String.IsNullOrEmpty(r.Field<string>("Description")) ? Helper.TrimFrontIfLongerThan(r.Field<string>("Description"), 100) : string.Empty,
                                                        avatarPath = r.Field<string>("AvatarPath"),
                                                        street = r.Field<string>("Street")
                                                    }).ToList();
            }


            if (!String.IsNullOrEmpty(message_code))
            {
                model.MessageCode = message_code;
                switch (message_code)
                { 
                    case Utilities.Constants.UnAuthorized:
                        model.Message = Utilities.Constants.UnAuthorized_Str;
                        break;
                    case Utilities.Constants.Verified:
                        model.Message = Utilities.Constants.Verified_Str;
                        break;
                    default:
                        break;
                }

            }
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Contact(ContactModel model)
        {
            var isSuccess = false;
            if (ModelState.IsValid)
            {
                var staffMailAccount = ConfigurationManager.AppSettings["staffMailAccount"];
                IdentityMessage message = new IdentityMessage
                {
                    Subject = Utilities.Constants.Contact_Us_Subject,
                    Destination = staffMailAccount,
                    Body = "<p style=\"font:normal 13px Arial;\"> Please see inquiry details below:" + "<br>" + 
                                    "<table>" +
                                    "Customer Name: " + model.Name + "<br>" + 
                                    "Customer Email: " + model.Email + "<br>"  +
                                    "Customer Message: " + model.Message + "<br></p>"  
                };

                await _emailHelper.SendAsync(message);
                isSuccess = true;
            }

            return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDispensaryLocations(string zipCode)
        {
            List<DispensaryZipModel> dispensaryList = new List<DispensaryZipModel>();
            var dispensaries = _dispensaryRepository.GetDispensariesByZipCode(zipCode);
            if (dispensaries != null)
            {
                dispensaryList = dispensaries.AsEnumerable()
                    .Select(r => new DispensaryZipModel 
                            { 
                                id = r.Field<int>("DispensaryId"), 
                                address = r.Field<string>("Street") + " " + r.Field<string>("City") + " " + r.Field<string>("State"), 
                                dispensaryName = r.Field<string>("DispensaryName"),
                                dispensaryDesc = !String.IsNullOrEmpty(r.Field<string>("Description")) ? Helper.TrimFrontIfLongerThan(r.Field<string>("Description"), 100) : string.Empty,
                                avatarPath = r.Field<string>("AvatarPath"),
                                street = r.Field<string>("Street")
                            })
                    .ToList();
            }

            return Json(new { dispensaryList = dispensaryList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PageNotFound()
        {
            return View();
        }
    }
}
