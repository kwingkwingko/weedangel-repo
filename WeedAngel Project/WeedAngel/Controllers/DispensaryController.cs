﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WeedAngel.Contracts;
using WeedAngel.Models;
using WeedAngel.Repositories;
using WeedAngel.Utilities;

namespace WeedAngel.Controllers
{
    public class DispensaryController : Controller
    {
        private readonly DispensaryRepository _dispensaryRepository;
        private readonly ProductRepository _productRepository;
        private readonly DropdownRepository _dropdownRepository;

        public DispensaryController()
        {
            string _connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            _dispensaryRepository = new DispensaryRepository(_connStr);
            _productRepository = new ProductRepository(_connStr);
            _dropdownRepository = new DropdownRepository(_connStr);
        }

        public ActionResult Index(bool myDispensary = false)
        {
            
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var sid = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                   .Select(c => c.Value).SingleOrDefault();
            DispensarySearchModel model = new DispensarySearchModel 
            { 
                UserId = sid,
                IsMyDispensary = myDispensary
            };
            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            var dispensaryModel = new DispensaryModel();
            dispensaryModel.TimeZoneList = _dropdownRepository.GetTimeZone();
            return View(dispensaryModel);
        }

        [HttpPost]
        public ActionResult Create(DispensaryModel dispensaryModel, HttpPostedFileBase files, List<HttpPostedFileBase> images, List<HttpPostedFileBase> videos)
        {
            Guid guid = Guid.NewGuid();
            var profileImgName = string.Empty;
            var profileFolderName = "Profile";
            var imagesFolderName = "Images";
            var videosFolderName = "Videos";
            var bucketName = ConfigurationManager.AppSettings["AWSDispensaryBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSDispensaryBucketPath"];

            if (ModelState.IsValid)
            {
                if (files != null && files.ContentLength > 0)
                {
                    // Upload image to amazon
                    profileImgName = Path.GetFileName(files.FileName);
                    AmazonHandler.UploadFileToAws(bucketName, guid.ToString(),profileFolderName, profileImgName, files.InputStream);

                    // Set db values
                    dispensaryModel.AvatarName = profileImgName;
                    dispensaryModel.AvatarPath = bucketPath + guid.ToString() + "/" + profileFolderName + "/" + profileImgName;
                }

                if (images != null && images.Count > 0)
                {
                    foreach (var image in images)
                    {
                        if (image != null)
                        {
                            var imageFileName = Path.GetFileName(image.FileName);
                            AmazonHandler.UploadFileToAws(bucketName, guid.ToString(), imagesFolderName, imageFileName, image.InputStream);
                            dispensaryModel.DispensaryImages += imageFileName + ";";
                        }
                    }
                }

                if (videos != null && videos.Count > 0)
                {
                    foreach (var video in videos)
                    {
                        if (video != null)
                        {
                            var videoFileName = Path.GetFileName(video.FileName);
                            AmazonHandler.UploadFileToAws(bucketName, guid.ToString(), videosFolderName, videoFileName, video.InputStream);
                        }
                    }
                }

                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

                // Get current user id
                var sid = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                   .Select(c => c.Value).SingleOrDefault();
                dispensaryModel.CreatedBy = Convert.ToInt32(sid);

                dispensaryModel.UniqueKey = guid;
                dispensaryModel.CreatedDateTime = DateTime.Now;

                if (!String.IsNullOrEmpty(dispensaryModel.DispensaryImages))
                    dispensaryModel.DispensaryImages = dispensaryModel.DispensaryImages.TrimEnd(';');
                _dispensaryRepository.CreateDispensary(dispensaryModel);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Create");
        }

        [Authorize]
        public ActionResult Edit(string dispensaryName)
        {

            var bucketName = ConfigurationManager.AppSettings["AWSDispensaryBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSDispensaryBucketPath"];
            var bucketNameArr = bucketName.Split('/');
            var imagesFolderName = "Images";
            var videosFolderName = "Videos";

            var model = _dispensaryRepository.GetDispensaryByName(dispensaryName);

            if (model != null)
            {
                // Check owner of dispensary
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                var sid = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                       .Select(c => c.Value).SingleOrDefault();

                if (Convert.ToInt32(sid) != model.CreatedBy.Value)
                {
                    return RedirectToAction("Detail", "Dispensary", new { dispensaryName = dispensaryName, unAuthorized = Constants.UnAuthorized });
                }
                    
                // Get attached videos and images
                var videoList = AmazonHandler.GetBucketKeyFiles(bucketNameArr[0], bucketNameArr[1], model.UniqueKey.ToString(), videosFolderName);
                if (!String.IsNullOrEmpty(model.DispensaryImages))
                {
                    model.DispensaryImages = model.DispensaryImages.TrimEnd(';');
                    var imageList = model.DispensaryImages.Split(';').Select(x => x.Trim());
                    model.ExistingImageFiles = HttpUtility.UrlDecode(String.Join(";", imageList));
                    foreach (var filename in imageList)
                    {
                        model.ImageSources += bucketPath + model.UniqueKey + "/" + imagesFolderName + "/" + filename + ";"; 
                    }

                    if (!String.IsNullOrEmpty(model.ImageSources))
                        model.ImageSources = model.ImageSources.TrimEnd(';');
                }

                if (videoList != null && videoList.Count > 0)
                {
                    var videoFilenames = videoList.Select(x => Helper.GetFilenameFromURI(x)).ToList();
                    model.ExistingVideoFiles = String.Join(";", videoFilenames);
                    model.videoSources = String.Join(";", videoList.ToArray());
                }
                    
                model.TimeZoneList = _dropdownRepository.GetTimeZone();
                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(DispensaryModel model, HttpPostedFileBase files, List<HttpPostedFileBase> images, List<HttpPostedFileBase> videos)
        {
            var profileImgName = string.Empty;
            var profileFolderName = "Profile";
            var imagesFolderName = "Images";
            var bucketName = ConfigurationManager.AppSettings["AWSDispensaryBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSDispensaryBucketPath"];
            var bucketNameArr = bucketName.Split('/');
            if (ModelState.IsValid)
            {
                if (model.IsProfileChanged && files.ContentLength > 0)
                {
                    // Delete profile image in amazon
                    AmazonHandler.DeleteObject(bucketNameArr[0], bucketNameArr[1], model.UniqueKey.ToString(), profileFolderName, model.AvatarName, bucketPath);
                    
                    // Upload image to amazon
                    profileImgName = Path.GetFileName(files.FileName);
                    AmazonHandler.UploadFileToAws(bucketName, model.UniqueKey.ToString(), profileFolderName, profileImgName, files.InputStream);

                    // Set db values
                    model.AvatarName = profileImgName;
                    model.AvatarPath = bucketPath + model.UniqueKey.ToString() + "/" + profileFolderName + "/" + profileImgName;
                }

                // Remove delete images
                if (!String.IsNullOrEmpty(model.ExistingImageFiles))
                {
                    var deletedImages = model.ExistingImageFiles.TrimEnd(';').Split(';');
                    
                    foreach (var image in deletedImages)
                    {
                        // Delete each images
                        AmazonHandler.DeleteObject(bucketNameArr[0], bucketNameArr[1], model.UniqueKey.ToString(), imagesFolderName, image, bucketPath);
                        model.DeletedImages += image + ";";
                    }
                }

                // Add new images
                if (images != null && images.Count > 0)
                {
                    model.DispensaryImages = string.Empty;
                    foreach (var image in images)
                    {
                        if (image != null)
                        {
                            var imageFileName = Path.GetFileName(image.FileName);
                            AmazonHandler.UploadFileToAws(bucketName, model.UniqueKey.ToString(), imagesFolderName, imageFileName, image.InputStream);
                            model.DispensaryImages += imageFileName + ";";
                        }
                    }
                }
            }

            // Trim semicolon in end of concatenated string
            if (!String.IsNullOrEmpty(model.DeletedImages))
                model.DeletedImages = model.DeletedImages.TrimEnd(';');

            if (!String.IsNullOrEmpty(model.DispensaryImages))
                model.DispensaryImages = model.DispensaryImages.TrimEnd(';');

            _dispensaryRepository.UpdateDispensary(model);
            return RedirectToAction("Detail", new { dispensaryName = model.DispensaryName });
        }

        public ActionResult Detail(string dispensaryName, string unAuthorized = null)
        {
            var model = _dispensaryRepository.GetDispensaryByName(dispensaryName);
            var bucketName = ConfigurationManager.AppSettings["AWSDispensaryBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSDispensaryBucketPath"];
            var bucketNameArr = bucketName.Split('/');
            var imagesFolderName = "Images";

            if (model != null)
            {
                var imagesPath = Server.MapPath(String.Format(Constants.Dispensary_Images_Path, model.UniqueKey));
                var videosPath = Server.MapPath(String.Format(Constants.Dispensary_Videos_Path, model.UniqueKey));

                // Format time values
                model.SunDayOpen = (!String.IsNullOrEmpty(model.SunDayOpen)) ? model.SunDayOpen.ToLower() : null;
                model.SunDayClose = (!String.IsNullOrEmpty(model.SunDayClose)) ? model.SunDayClose.ToLower() : null;
                model.MonDayOpen = (!String.IsNullOrEmpty(model.MonDayOpen)) ? model.MonDayOpen.ToLower() : null;
                model.MonDayClose = (!String.IsNullOrEmpty(model.MonDayClose)) ? model.MonDayClose.ToLower() : null;
                model.TuesDayOpen = (!String.IsNullOrEmpty(model.TuesDayOpen)) ? model.TuesDayOpen.ToLower() : null;
                model.TuesDayClose = (!String.IsNullOrEmpty(model.TuesDayClose)) ? model.TuesDayClose.ToLower() : null;
                model.WednesDayOpen = (!String.IsNullOrEmpty(model.WednesDayOpen)) ? model.WednesDayOpen.ToLower() : null;
                model.WednesDayClose = (!String.IsNullOrEmpty(model.WednesDayClose)) ? model.WednesDayClose.ToLower() : null;
                model.ThursDayOpen = (!String.IsNullOrEmpty(model.ThursDayOpen)) ? model.ThursDayOpen.ToLower() : null;
                model.ThursDayClose = (!String.IsNullOrEmpty(model.ThursDayClose)) ? model.ThursDayClose.ToLower() : null;
                model.FriDayOpen = (!String.IsNullOrEmpty(model.FriDayOpen)) ? model.FriDayOpen.ToLower() : null;
                model.FriDayClose = (!String.IsNullOrEmpty(model.FriDayClose)) ? model.FriDayClose.ToLower() : null;
                model.SaturDayOpen = (!String.IsNullOrEmpty(model.SaturDayOpen)) ? model.SaturDayOpen.ToLower() : null;
                model.SaturDayClose = (!String.IsNullOrEmpty(model.SaturDayClose)) ? model.SaturDayClose.ToLower() : null;
                model.Address = model.Street + ", " + model.City + ", " + model.State + " " + model.Zipcode;
                
                var imageList = AmazonHandler.GetBucketKeyFiles(bucketNameArr[0], bucketNameArr[1], model.UniqueKey.ToString(), imagesFolderName);
                if (imageList != null && imageList.Count > 0)
                {                
                    model.ImageSrcList = imageList;
                }

                if (!String.IsNullOrEmpty(unAuthorized))
                    model.errorText = unAuthorized;

                return View(model);
            }

            return RedirectToAction("Index", "Dispensary");
        }

        public ActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                _dispensaryRepository.DeleteDispensary(1);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        #region -- Product Methods --
        [Authorize]
        public JsonResult AddProduct(ProductModel product)
        {
            var isSuccess = false;
            var bucketName = ConfigurationManager.AppSettings["AWSProductBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSProductBucketPath"];

            if (product != null)
            {
                try
                {
                    if (Request.Files.Count > 0)
                    {
                        foreach (string file in Request.Files)
                        {
                            var fileContent = Request.Files[file];
    
                            // Upload image to amazon
                            var productImgName = Path.GetFileName(file);
                            AmazonHandler.UploadFileToAws(bucketName, string.Empty, product.ProductDispensaryId.ToString(), productImgName, fileContent.InputStream);

                            // Set DB Values
                            product.FileName = productImgName;
                            product.FilePath = bucketPath + product.ProductDispensaryId.ToString() + "/" + productImgName;
                        }
                    }

                    _productRepository.CreateProduct(product);
                    isSuccess = true;
                }
                catch (Exception ex) { isSuccess = false; }
            }

            return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult EditProduct(ProductModel product)
        {
            var isSuccess = false;
            var dispensaryModel = _dispensaryRepository.GetDispensaryById(product.ProductDispensaryId);
            var bucketName = ConfigurationManager.AppSettings["AWSProductBucket"];
            var bucketPath = ConfigurationManager.AppSettings["AWSProductBucketPath"];
            var bucketNameArr = bucketName.Split('/');

            if (dispensaryModel != null && product.ProductId > 0)
            {
                // Check owner of dispensary
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                var sid = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
                                       .Select(c => c.Value).SingleOrDefault();

                if (Convert.ToInt32(sid) != dispensaryModel.CreatedBy.Value)
                {
                    return RedirectToAction("Detail", "Dispensary", new { dispensaryName = dispensaryModel.DispensaryName, unAuthorized = Constants.UnAuthorized_Str });
                }

                var productImgFolder = Server.MapPath(String.Format(Constants.Product_Img_Path, product.ProductDispensaryId));
                if (product != null)
                {
                    try
                    {
                        if (Request.Files.Count > 0)
                        {
                            foreach (string file in Request.Files)
                            {
                                var fileContent = Request.Files[file];

                                // Delete profile image in amazon
                                AmazonHandler.DeleteObject(bucketNameArr[0], bucketNameArr[1], string.Empty, product.ProductDispensaryId.ToString(), product.FileName, bucketPath);

                                // Upload image to amazon
                                var productImgName = Path.GetFileName(fileContent.FileName);
                                AmazonHandler.UploadFileToAws(bucketName, string.Empty, product.ProductDispensaryId.ToString(), productImgName, fileContent.InputStream);

                                product.FileName = productImgName;
                                product.FilePath = bucketPath + product.ProductDispensaryId.ToString() + "/" + productImgName;
                            }
                        }

                        _productRepository.UpdateProduct(product);
                        isSuccess = true;
                    }
                    catch (Exception ex) { isSuccess = false; }
                }
                return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }
        #endregion

        public JsonResult GetDispensaries(DispensaryFilterModel filters)
        {
            filters.DispensaryName = filters.DispensaryName ?? string.Empty;
            filters.StartLimit = filters.PageSize * filters.PageIndex + 1;
            filters.EndLimit = filters.PageSize * filters.PageIndex + filters.PageSize;

            var dtDispensaryList = _dispensaryRepository.GetDispensary(filters);

            foreach (DataRow row in dtDispensaryList.Rows)
            {
                row["Description"] = Helper.TrimFrontIfLongerThan(row["Description"].ToString(), 100);
            }

            return Json(new
            {
                data = JsonConvert.SerializeObject(dtDispensaryList)
            },
            JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadProducts(int dispensaryId, string productName)
        {
            var categories = _productRepository.GetProducts(dispensaryId, productName);
            var productImgFolder = String.Format(Constants.Product_Img_Path, dispensaryId);

            List<ProductListModel> productList = new List<ProductListModel>();

            foreach (var row in categories.AsEnumerable())
            {
                var categoryId = row.Field<string>("ProductTypeDesc");
                var createdBy = row.Field<int>("CreatedBy");
                var product = new ProductModel
                {
                    ProductId = row.Field<int>("ProductId"),
                    ProductName = row.Field<string>("Name"),
                    Description = row.Field<string>("Description"),
                    CategoryTypeId = row.Field<int>("ProductTypeId"),
                    Price1 = Convert.ToDouble(row.Field<decimal?>("Price1")),
                    Price2 = Convert.ToDouble(row.Field<decimal?>("Price2")),
                    Price3 = Convert.ToDouble(row.Field<decimal?>("Price3")),
                    Price4 = Convert.ToDouble(row.Field<decimal?>("Price4")),
                    Price5 = Convert.ToDouble(row.Field<decimal?>("Price5")),
                    Each = Convert.ToDouble(row.Field<decimal?>("Each")),
                    FileName = row.Field<string>("Filename"),
                    FilePath = row.Field<string>("FilePath"),
                    ProductDispensaryId = row.Field<int>("DispensaryID"),
                    ProductCategoryName = row.Field<string>("ProductTypeDesc")
                };

                // Build each category
                if (!productList.Any(x => x.CategoryName == categoryId))
                {
                    var itemList = new List<ProductModel>();

                    itemList.Add(product);
                    productList.Add(new ProductListModel { CategoryName = categoryId, prodList = itemList, CreatedBy = createdBy });
                }
                else
                {
                    var list = productList.Where(x => x.CategoryName == categoryId).FirstOrDefault();
                    list.prodList.Add(product);
                }
            }
            return PartialView("Product/_ProductList", productList);
        }

        public JsonResult CheckDuplicate(int dispensaryId, string dispensaryName)
        {
            var isExists = false;

            var model = _dispensaryRepository.GetDispensaryByName(dispensaryName);
            if (model != null && !String.IsNullOrEmpty(model.DispensaryName))
                if (model.DispensaryId != dispensaryId)
                    isExists = true;
            return Json(new {isExists = isExists}, JsonRequestBehavior.AllowGet);
        }

    }
}
