﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeedAngel.Models;

namespace WeedAngel.Contracts
{
    interface IDispensaryRepository
    {
        DataTable GetDispensary(DispensaryFilterModel filters);
        DispensaryModel GetDispensaryById(int dispensaryId);
        void CreateDispensary(DispensaryModel dispensaryModel);
        void UpdateDispensary(DispensaryModel dispensaryModel);
        void DeleteDispensary(int dispensaryId);
    }
}
