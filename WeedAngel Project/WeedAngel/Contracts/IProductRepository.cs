﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeedAngel.Models;

namespace WeedAngel.Contracts
{
    interface IProductRepository
    {
        DataTable GetProducts();
        ProductModel GetProductById(int productId);
        void CreateProduct(ProductModel product);
        void UpdateProduct(ProductModel product);
        void DeleteProduct(int productId);
        
    }
}
