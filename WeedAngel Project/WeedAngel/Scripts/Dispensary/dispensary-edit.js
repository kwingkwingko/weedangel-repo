﻿var markers = [];
$(document).ready(function () {
    initialize();
    loadServerImages();
    $('.clockpicker').clockpicker({ donetext: 'Done', twelvehour: true });

    $("#cancelBtn").on('click', function () {
        document.location = '@Url.Action("Index","Dispensary")';
    });

    $('#profileImage').on('change', function () {
        $('#IsProfileChanged').attr("value", "True");
    });

    $('.remove-exist').on('click', function () {
        var filename = $(this).prev('.existing-filename').val();
        var deletedImages = $('#deleted-images');
        deletedImages.val(deletedImages.val() + filename + ';');
        $(this).closest('.MultiFile-label').remove();
    });

    var validate = $("#editForm").validate({
        debug: false,
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            "DispensaryName": {
                required: true
            },
            "Street": {
                required: true
            },
            "City": {
                required: true
            },
            "State": {
                required: true
            },
            "Zipcode": {
                required: true
            },
        },
        messages: {
            "DispensaryName": {
                required: "Dispensary Name is required"
            },
            "Street": {
                required: "Street is required"
            },
            "City": {
                required: "City is required"
            },
            "State": {
                required: "State is required"
            },
            "Zipcode": {
                required: "Zipcode is required"
            },
        },
        submitHandler: function (form) {
            var deletedImages = $('#deleted-images').val();
            $('#ExistingImageFiles').val(deletedImages);
            
            $.ajax({
                type: 'POST',
                url: '/Dispensary/CheckDuplicate',
                dataType: 'json',
                data: {
                    'dispensaryId': $('#DispensaryId').val(),
                    'dispensaryName': $('#DispensaryName').val(),
                },
                success: function (data) {
                    if (data.isExists) {
                        validate.showErrors({
                            "DispensaryName": "Dispensary name already exists."
                        });

                        $("#DispensaryName").focus();
                    }
                    else {
                        form.submit();
                    }
                }
            });
        },
    });

    $('#Street, #City, #State').on('blur', function () {
        var street = $.trim($('#Street').val());
        var city = $.trim($('#City').val());
        var state = $.trim($('#State').val());

        if (street !== '' && city !== '' && state !== '') {
            removeAllMarkers();
            var address = street + " " + city + " " + state;

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });

                    markers.push(marker);
                }
            });
        }
    });
});

function removeAllMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}

function initialize() {
    var street = $.trim($('#Street').val());
    var city = $.trim($('#City').val());
    var state = $.trim($('#State').val());

    if (street !== '' && city !== '' && state !== '') {
        removeAllMarkers();
        var address = street + " " + city + " " + state;

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                markers.push(marker);
            }
        });
    }
}

function loadServerImages()
{
    var imageSources = $('#ImageSources').val();
    var existingImages = $('#ExistingImageFiles').val();
    var partsOfStr = imageSources.split(';');
    var filenames = existingImages.split(';');

    $(partsOfStr).each(function (index, value) {
        if (value !== '' && value != 'undefined')
            var newImage = '<div class="MultiFile-label">' +
                                '<input class="existing-filename" type="hidden" value="'+ filenames[index] + '" />' +
                                '<a class="MultiFile-remove remove-exist" >x</a>' +
                                '<span>' +
                                    '<span class="MultiFile-label" title="">' +
                                        '<span class="MultiFile-title">' + filenames[index] + '</span>' +
                                        '<img class="MultiFile-preview" style="max-height:100px; max-width:100px;" src="' + value + '">' +
                                    '</span>' +
                                '</span>' +
                            '</div>';
        $('.file-upload-previews').append(newImage);
    });
}