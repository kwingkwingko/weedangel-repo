﻿var markers = [];
$(document).ready(function () {
    initialize();
    $('.clockpicker').clockpicker({ donetext: 'Done', twelvehour: true });
    
    $("#cancelBtn").on('click', function () {
        document.location = '@Url.Action("Index","Dispensary")';
    });

    var validate = $("#createForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        onfocusout: false,
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {                    
                validator.errorList[0].element.focus();
            }
        }, 
        rules: {
            "DispensaryName": {
                required: true
            },
            "Street": {
                required: true
            },
            "City": {
                required: true
            },
            "State": {
                required: true
            },
            "Zipcode": {
                required: true
            },
        },
        messages: {
            "DispensaryName": {
                required: "Dispensary Name is required"
            },
            "Street": {
                required: "Street is required"
            },
            "City": {
                required: "City is required"
            },
            "State": {
                required: "State is required"
            },
            "Zipcode": {
                required: "Zipcode is required"
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: 'POST',
                url: '/Dispensary/CheckDuplicate',
                dataType: 'json',
                data: {
                    'dispensaryId': 0,
                    'dispensaryName': $('#DispensaryName').val(),
                },
                success: function (data) {
                    if (data.isExists) {
                        validate.showErrors({
                            "DispensaryName": "Dispensary name already exists."
                        });

                        $("#DispensaryName").focus();
                    }
                    else {
                        form.submit();
                    }
                }
            });
        },
    });

    $('#Street, #City, #State').on('blur', function () {
        var street = $.trim($('#Street').val());
        var city = $.trim($('#City').val());
        var state = $.trim($('#State').val());

        if (street !== '' && city !== '' && state !== '') {
            removeAllMarkers();
            var address = street + " " + city + " " + state;

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });

                    markers.push(marker);
                }
            });
        }
    });
});

function removeAllMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}

function initialize() {
    var _latitude;
    var _longitude;

    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            _latitude = position.coords.latitude;
            _longitude = position.coords.longitude;

            var element = "map-item";
            var useAjax = true;
            bigMap(_latitude, _longitude, element, useAjax);
        });
    }
}

function OnCreateSuccess(data)
{

}