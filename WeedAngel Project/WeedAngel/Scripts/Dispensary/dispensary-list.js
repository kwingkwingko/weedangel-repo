﻿var pageSize = 9;
var pageIndex = 0;
$(function () {
    GetDispensaries();

    $('#loadMore').on('click', function () {
        GetDispensaries();
    });

    $('#searchBtn').on('click', function (e) {
        e.preventDefault();
        pageSize = 9;
        pageIndex = 0;
        $('#displayArea').html('');
        GetDispensaries();
    });

    $('#clearBtn').on('click', function () {
        $('#dispensaryName').val('');
        pageSize = 9;
        pageIndex = 0;
        $('#displayArea').html('');
        GetDispensaries();
    });
});

function GetDispensaries() {
    var dispensaryName = $('#dispensaryName').val();
    var userId = $('#UserId').val();
    var myDispensary = $('#IsMyDispensary').val();

    $.ajax({
        type: 'GET',
        url: '/Dispensary/GetDispensaries',
        data: {
            'DispensaryName': dispensaryName,
            'PageSize': pageSize,
            'PageIndex': pageIndex,
            'UserId': userId,
            'IsMyDispensary': myDispensary
        },
        success: function (result) {
            if (result.data != null) {
                pageIndex++;

                $.each(JSON.parse(result.data), function (key, value) {
                    var filePath = (!value.AvatarPath) ? '/Content/assets/img/items/01.jpg' : value.AvatarPath;
                    var description = (!value.Description) ? '&nbsp;' : value.Description;
                    var product = '<div class="col-md-4 col-sm-6">' +
                                    '<div class="item big equal-height" data-map-latitude="48.87" data-map-longitude="2.29" data-id="1" style="height: 350px;">' +
                                        '<div class="item-wrapper">' +
                                            '<div class="image">' +
                                                '<a style="text-align:center;" href="' + baseUrl + 'Dispensary/Detail/' + value.DispensaryName + '" class="wrapper">' +
                                                    '<div style="width:100%; height:inherit;" class="gallery">' +
                                                        '<img style="width:inherit; height:inherit;" src="' + filePath + '" alt="">' +
                                                    '</div>' +
                                                '</a>' +
                                            '</div>' +
                                            '<div class="description">' +
                                                '<div class="info">' +
                                                    '<figure class="label label-info">' + value.Street + '</figure>' +
                                                    '<a href="' + baseUrl + 'Dispensary/Detail/' + value.DispensaryName + '"><h3>' + value.DispensaryName + '</h3></a>' +
                                                    '<figure class="location" style="text-align:center;">' + description + '</figure>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="map-item">' +
                                                '<button class="btn btn-close"><i class="fa fa-close"></i></button>' +
                                                '<div class="map-wrapper"></div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>';
                    $('#displayArea').append(product);
                });
            }
        },
        error: function () {
            //alert('Error while retrieving data!');
        }
    });
}