﻿$(function () {
    $("#ProductDispensaryId").val($('#DispensaryId').val());
    initialize();
    LoadProducts();

    // Display unauthorized access
    var errorText = $('#errorText').val();
    if (errorText === 'unauthorized')
        $('#unauthorized-pop').removeClass('hidden').fadeOut(5000);

    $(document.body).on('hidden.bs.modal', function (e) {
        var modal = e.target.id;

        if (modal === 'add-product-modal') {
            resetForm();
        }
    });

    $(document).on("click", "#editProductBtn", function (e) {
        var productId = $(this).data('prod-id');
        var imagePath = $(this).data('prod-path');
        var filename = $(this).data('prod-filename');
        var productName = $(this).data('prod-name');
        var prodCategory = $(this).data('prod-category');
        var description = $(this).data('prod-desc');
        var price1 = $(this).data('prod-price1');
        var price2 = $(this).data('prod-price2');
        var price3 = $(this).data('prod-price3');
        var price4 = $(this).data('prod-price4');
        var price5 = $(this).data('prod-price5');
        var priceEach = $(this).data('prod-priceeach');
        var prodDispensary = $(this).data('prod-dispensaryid');

        $('#addProductForm').attr('action', '/Dispensary/EditProduct');
        $('#addProductForm #ProductDispensaryId').val(prodDispensary);
        $("#addProductForm #ProductId").val(productId);
        $("#addProductForm #ProductName").val(productName);
        $("#addProductForm #CategoryTypeId").val(prodCategory);
        $("#addProductForm #Description").val(description);
        $("#addProductForm #Price1").val(price1);
        $("#addProductForm #Price2").val(price2);
        $("#addProductForm #Price3").val(price3);
        $("#addProductForm #Price4").val(price4);
        $("#addProductForm #Price5").val(price5);
        $("#addProductForm #Each").val(priceEach);
        $("#addProductForm #FileName").val(filename);
        $("#addProductForm .image").attr('src', imagePath.replace('~', ''));
        $("#submitBtn").html("Save");
    });

    $(document).on("click", "#viewProdBtn", function (e) {
        var productId = $(this).data('prod-id');
        var imagePath = $(this).data('prod-detail-path');
        var filename = $(this).data('prod-detail-filename');
        var productName = $(this).data('prod-detail-name');
        var prodCategory = $(this).data('prod-detail-category');
        var description = $(this).data('prod-detail-desc');
        var price1 = $(this).data('prod-detail-price1');
        var price2 = $(this).data('prod-detail-price2');
        var price3 = $(this).data('prod-detail-price3');
        var price4 = $(this).data('prod-detail-price4');
        var price5 = $(this).data('prod-detail-price5');
        var priceEach = $(this).data('prod-detail-priceeach');

        $("#prodDetailForm #ProdDetailName").val(productName);
        $("#prodDetailForm #ProdDetailCat").val(prodCategory);
        $("#prodDetailForm #ProdDetailDesc").val(description);
        $("#prodDetailForm .image").attr('src', imagePath.replace('~', ''));

        if (prodCategory != 'Edible' && prodCategory != 'Topicals' && prodCategory != 'Gear') {
            $('#edible-detail').hide();
            $('#non-edible-detail').show();
            $("#prodDetailForm #ProdDetailPrice1").val(price1);
            $("#prodDetailForm #ProdDetailPrice2").val(price2);
            $("#prodDetailForm #ProdDetailPrice3").val(price3);
            $("#prodDetailForm #ProdDetailPrice4").val(price4);
            $("#prodDetailForm #ProdDetailPrice5").val(price5);
        }
        else {
            $('#edible-detail').show();
            $('#non-edible-detail').hide(); 
            $("#prodDetailForm #ProdDetailEach").val(priceEach);
        }
    });

    $('#add-product-modal').on('shown.bs.modal', function () {
        var category = $("#CategoryTypeId option:selected").text();
        if (category == 'Edible' || category == 'Topicals' || category == 'Gear') {
            $('#edible').show();
            $('#non-edible').hide();
        }
        else {
            $('#edible').hide();
            $('#non-edible').show();
        }
    });

    $(document).on("change", "#CategoryTypeId", function (e) {
        var category = $("#CategoryTypeId option:selected").text();
        if (category == 'Edible' || category == 'Topicals' || category == 'Gear') {
            $('#edible').show();
            $('#non-edible').hide();
            $("#addProductForm #Each").val('');
        }
        else {
            $('#edible').hide();
            $('#non-edible').show();
            $("#addProductForm #Price1").val('');
            $("#addProductForm #Price2").val('');
            $("#addProductForm #Price3").val('');
            $("#addProductForm #Price4").val('');
            $("#addProductForm #Price5").val('');
        }
    });

    $('#searchProdBtn').click(function (e) {
        e.preventDefault();
        LoadProducts();
    });

    $('#clearBtn').click(function () {
        $('#ProductNameTxt').val('');
        LoadProducts();
    });

    //Social Media Accounts
    $("#link-facebook").click(function () {
        window.open($("#Facebook").val())
    });
    $("#link-twitter").click(function () {
        window.open($("#Twitter").val())
    });
    $("#link-instagram").click(function () {
        window.open($("#Instagram").val())
    });
    $("#link-website").click(function () {
        window.open($("#Website").val())
    });

    $("#cancelBtn").click(function () {
        validator.resetForm();
    });

    $("#directionResults").hide();
    $("#btnDirection").click(function () {
        if ($("#btnDirection").text() == 'Show Directions') {
            $("#directionResults").show();
            $("#btnDirection").text('Hide Directions');
        }
        else {
            $("#directionResults").hide();
            $("#btnDirection").text('Show Directions');
        }
    });

    var validator = $("#addProductForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            "ProductName": {
                required: true
            },
            "Description": {
                required: true
            },
            "CategoryTypeId": {
                required: true
            },
        },
        messages: {
            "ProductName": {
                required: "Product Name is required"
            },
            "Description": {
                required: "Description is required"
            },
            "CategoryTypeId": {
                required: "Product Category is required"
            },
        },
        submitHandler: function (form) {
            $('#addProductForm').unbind('submit');
            $('#addProductForm').on('submit',function (e) {
                e.preventDefault();
                var fileUpload = $("#productImg").get(0);
                var files = fileUpload.files;
                var action = $("#addProductForm").attr('action').replace('/', '');
                var formData = new FormData();

                // Looping over all files and add it to FormData object  
                for (var i = 0; i < files.length; i++) {
                    formData.append(files[i].name, files[i]);
                }

                var other_data = $('#addProductForm').serializeArray();
                $.each(other_data, function (key, input) {
                    formData.append(input.name, input.value);
                });

                $.ajax({
                    type: 'POST',
                    url: baseUrl + action,
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data, xhr) {
                        ProductSuccess(data);
                    },
                    error: function (httpObj, textStatus)
                    {
                        if (httpObj.status == 401 && httpObj.statusText == 'Unauthorized') {
                            window.location = baseUrl + 'Home/Index?error=unauthorized';
                        }
                    },
                });
            });

            return false;
        },
    });

    function ProductSuccess(data) {
        if (data.isSuccess) {
            resetForm();
            LoadProducts();
            validator.resetForm();
        }
    }

});
var address;
function initialize() {
    var street = $.trim($('#Street').val());
    var city = $.trim($('#City').val());
    var state = $.trim($('#State').val());

    if (street !== '' && city !== '' && state !== '') {
        address = street + " " + city + " " + state;

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });

                directionsService = new google.maps.DirectionsService();
                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch({
                    location: results[0].geometry.location,
                    radius: 5500,
                }, callback);
            }
        });
    }
}

function callback(results, status) {
    var direction_num_limit = 4;
    var directionsDisplay = $('#nearbyPlacesDisplay');
    directionsDisplay.empty();

    if (status === google.maps.places.PlacesServiceStatus.OK) {

        for (var i = 0; i < results.length && i < direction_num_limit; i++) {
            var geocoder = new google.maps.Geocoder();
           
            (function(){
                var name = results[i].name;

                geocoder.geocode({ 'latLng': results[i].geometry.location }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var add = results[0].formatted_address;

                        // Build direction request
                        var request = {
                            origin: add,
                            destination: address,
                            travelMode: 'DRIVING'
                        };

                        var directionsService = new google.maps.DirectionsService();;
                        directionsService.route(request, function (response, status) { // get driving direction steps
                            if (status == "OK") {
                                var myRoute = response.routes[0].legs[0];
                                var article = $('<form><table class="table"><tbody><tr class="room"></tr> </tbody></table></form>')
                                var directions_header = $('<h3>From ' + name + '</h3>')
                                var directions_parag = $('<p></p>')

                                for (var i = 0; i < myRoute.steps.length; i++) {
                                    directions_parag.append(myRoute.steps[i].instructions + '<br />');
                                }

                                // Append to DOM
                                article.append(directions_header);
                                article.append(directions_parag);
                                directionsDisplay.append(article);
                            }
                        });
                    }
                });
            })();
        }
    }
}

function LoadProducts() {
    $.ajax({
        type: 'GET',
        url: '/Dispensary/LoadProducts',
        dataType: 'html',
        data: {
            'dispensaryId': $('#DispensaryId').val(),
            'productName': $('#ProductNameTxt').val()
        },
        success: function (data) {
            $('#productListDisplay').empty();
            $('#productListDisplay').html(data);
        }
    });
}

function resetForm() {
    $("#submitBtn").html("Add");
    $('#addProductForm').reset();
    $('#addProductForm').attr('action', '/Dispensary/AddProduct');
    $('#addProductForm .image').attr('src', '/Content/assets/img/person-01.jpg');
    $('#add-product-modal').modal('hide');

}