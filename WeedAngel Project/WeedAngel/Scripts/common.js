﻿$(function () {
    $('#registerLnk').on('click', function () {
        $('#sign-in-modal').modal('hide');
        $('#register-modal').modal('show');
    });

    $('#forgotPassLnk').on('click', function () {
        $('#sign-in-modal').modal('hide');
    });

    $('#loginForm').validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            "UserName": {
                required: true
            },
            "Password": {
                required: true
            },
        },
        messages: {
            "UserName": {
                required: "Username is required"
            },
            "Password": {
                required: "Password is required"
            },
        },
    });

    var validateRegister = $('#registerForm').validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            "UserName": {
                required: true
            },
            "Email": {
                required: true,
                email: true
            },
            "FirstName": {
                required: true
            },
            "Surname": {
                required: true
            },
            "DummyPassword": {
                required: true,
            },
            "ConfirmPassword": {
                required: true,
                equalTo: "#DummyPassword"
            }
        },
        messages: {
            "UserName": {
                required: "Username is required"
            },
            "FirstName": {
                required: "Firstname is required"
            },
            "Surname": {
                required: "Lastname is required"
            },
            "Email": {
                required: "Email is required"
            },
        },
    });

    $("#contact-form").validate({
        errorClass: "my-error-class",
        onfocusout: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            "Name": {
                required: true
            },
            "Email": {
                required: true,
                email: true
            },
            "Message": {
                required: true
            },
        },
        messages: {
            "Name": {
                required: "Name is required"
            },
            "Email": {
                required: "Email is required"
            },
            "Message": {
                required: "Message is required"
            },
        }
    });

    $('.alert a').click(function () {
        $(this).parent().addClass('hidden');
    });

    $('#cancelRegisterBtn').on('click', function () {
        $('#registerForm').reset();
    });

    $('#sign-in-modal').on('hidden.bs.modal', function () {
        $('#sign-in-error').addClass('hidden');
        $('#sign-in-success').addClass('hidden');
    })

    $('#register-modal').on('hidden.bs.modal', function () {
        validateRegister.resetForm();
    })

    $('#forgot-password-modal').on('hidden.bs.modal', function () {
        $('#forgot-password-error').addClass('hidden');
        $('#forgot-password-success').addClass('hidden');
    })
}); 

function OnLoginSuccess(data) {
    $('#loginForm').reset();

    if (data.isSuccess) {
        $('#sign-in-error').addClass('hidden');
        $('#sign-in-modal').modal('hide');

        if (data.url !== '/')
            window.location.href = data.url;
        else
            location.reload();
    }
    else {
        $('#sign-in-success').addClass('hidden');
        $('#sign-in-error').removeClass('hidden');
        $('#sign-in-error strong').html(data.message);
    }
}

function OnRegisterSuccess(data) {
    $('#registerForm').reset();
    $('#register-modal').modal('hide');
    $('#sign-in-modal').modal('show');
}


function OnResetSuccess(data){
    if (data.isSuccess) {
        $('#forgot-password-success').removeClass('hidden');
        $('#forgot-password-error').addClass('hidden');
        $('#forgot-password-success strong').html(data.message);
    }
    else {
        $('#forgot-password-error').removeClass('hidden');
        $('#forgot-password-success').addClass('hidden');
        $('#forgot-password-error strong').html(data.message);
    }
}

function OnContactSuccess(data) {
    if (data.isSuccess) {
        $('#Name').val('');
        $('#Email').val('');
        $('#Message').val('');
        $('#contact-success').removeClass('hidden').fadeOut(5000);
    }
}