﻿var _latitude;
var _longitude;
var _currentLatLng;
var _nearestMarkerLatLng;
var _nearestDistance = 0;

$(function () {
    initialize();
    $("#form-hero").on('submit', function (e) {
        e.preventDefault();
        var zipCode = $('#zipCode').val();
        $.ajax({
            type:'GET',
            url: '/Home/GetDispensaryLocations',
            data: {
                'zipCode': zipCode
            },
            success: function (data) {;
                $('#show-map').trigger('click');
                setGeoLocation(data.dispensaryList);
                displayTopDispensaries(data.dispensaryList);
            }
        });
    });

    $('#show-map').click(function () {
        if ($('#show-map').text() == 'Hide Map') {
            $('#show-map').text('Show Map')
        }
        else {
            $('#show-map').text('Hide Map')
        }
    });
    
    checkLoginMessages();
});

function initialize() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            _latitude = position.coords.latitude;
            _longitude = position.coords.longitude;

            var element = "map-item-home";
            var useAjax = true;
            bigMap(_latitude, _longitude, element, useAjax);

            var latlng = new google.maps.LatLng(_latitude, _longitude);
            var geocoder = new google.maps.Geocoder();
            _currentLatLng = new google.maps.LatLng(_latitude, _longitude);

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        for (j = 0; j < results[0].address_components.length; j++) {
                            if (results[0].address_components[j].types[0] == 'postal_code') {
                                var zipCode = results[0].address_components[j].short_name;

                                if (zipCode !== '' && zipCode !== 'undefined') {
                                    //$('#zipCode').val(zipCode);
                                    //$("#form-hero").submit();
                                }
                            }
                        }
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });
        });
    }
}

function checkLoginMessages() {
    var message = $('#Message').val();
    var messageCode = $('#MessageCode').val();

    if (message != undefined && message != '') {
        switch (messageCode) {
            case 'unauthorized':
                $('#sign-in-success').addClass('hidden');
                $('#sign-in-error').removeClass('hidden');
                $('#sign-in-error > strong').html(message);
                break;
            case 'verified':
                $('#sign-in-success').removeClass('hidden');
                $('#sign-in-error').addClass('hidden');
                $('#sign-in-success > strong').html(message);
                break;
        }
        $('#sign-in-modal').modal('show');
    }
}

function displayTopDispensaries(dispensaryList) {

    if (dispensaryList != null && dispensaryList != undefined) {
        var displayArea = $('#top-dispensaries');
        displayArea.empty();

        $.each(dispensaryList, function (index, dispensary) {
            var filePath = (!dispensary.avatarPath) ? '/Content/assets/img/items/01.jpg' : dispensary.avatarPath;
            var description = (!dispensary.dispensaryDesc) ? '&nbsp;' : dispensary.dispensaryDesc;
            var markup = '<div class="col-md-3 col-sm-6">' +
                            '<div class="item big equal-height" style="height: 350px;">' +
                                '<div class="item-wrapper">' +
                                    '<div class="image">' +
                                        '<a href="' + baseUrl + 'Dispensary/Detail/' + dispensary.dispensaryName + '" class="wrapper">' +
                                            '<div style="width:100%; height:inherit;" class="gallery">' +
                                                '<img style="width:inherit; height:inherit;" src="' + filePath + '" alt="">' +
                                            '</div>' +
                                        '</a>' +
                                    '</div>' +
                                    '<div class="description">' +
                                        '<div class="info">' +
                                            '<figure class="label label-info">' + dispensary.street + '</figure>' +
                                            '<a href="' + baseUrl + 'Dispensary/Detail/' + dispensary.dispensaryName + '" target="_blank"><h3>' + dispensary.dispensaryName + '</h3></a>' +
                                            '<figure class="location">' + description + '</figure>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="map-item">' +
                                        '<button class="btn btn-close"><i class="fa fa-close"></i></button>' +
                                        '<div class="map-wrapper"></div>' +
                                    '</div>' +
                                    '</div>' +
                              '</div>' +
                         '</div>';
            displayArea.append(markup);
        });
    }
}

function setGeoLocation(addressList) {
    _nearestDistance = 0;
    removeMarkers()
    placeMarkers(addressList);
}
